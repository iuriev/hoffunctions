//var logic = require('./script.js');
var assert = require("chai").assert;

var data = [
    {
        "balance": "$2,499.49",
        "picture": "http://placehold.it/32x32",
        "age": 31,
        "name": "Lillian",
        "email": "lillianburgess@luxuria.com"
    },
    {
        "balance": "$1,972.47",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "name": "Marsh",
        "email": "marshmccall@ultrimax.com"
    },
    {
        "balance": "$3,946.45",
        "picture": "http://placehold.it/32x32",
        "age": 23,
        "name": "Bird",
        "email": "birdramsey@nimon.com"
    },
];
var sortResult = [
    {
        "balance": "$3,946.45",
        "picture": "http://placehold.it/32x32",
        "age": 23,
        "name": "Bird",
        "email": "birdramsey@nimon.com"
    },
    {
        "balance": "$2,499.49",
        "picture": "http://placehold.it/32x32",
        "age": 31,
        "name": "Lillian",
        "email": "lillianburgess@luxuria.com"
    },
    {
        "balance": "$1,972.47",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "name": "Marsh",
        "email": "marshmccall@ultrimax.com"
    }
];

var filterResult = [

    {
        "balance": "$2,499.49",
        "picture": "http://placehold.it/32x32",
        "age": 31,
        "name": "Lillian",
        "email": "lillianburgess@luxuria.com"
    }
];

var foreachResult = [
    {
        "balance": "$3,946.45",
        "picture": "http://placehold.it/32x32",
        "age": 23,
        "name": "Birdnew",
        "email": "birdramsey@nimon.com"
    },
    {
        "balance": "$2,499.49",
        "picture": "http://placehold.it/32x32",
        "age": 31,
        "name": "Lilliannew",
        "email": "lillianburgess@luxuria.com"
    },
    {
        "balance": "$1,972.47",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "name": "Marshnew",
        "email": "marshmccall@ultrimax.com"
    }
];

var mapCallback = function (a) {
    return a.name.toUpperCase();
};

var sortCallback = function (a, b) {
    if (a.name > b.name) {
        return 1;
    } else {
        return -1;
    }
};

var filterCallback = function (a) {
    return a.age > 28;
};

var reduceCallback = function myReduce(prev, item) {
    return prev + item.name.length;
};

var foreachCallback = function (item) {
    item.name = item.name + "new";
};

// this foreache work with dom elements accotrding to task
// var foreachCallback = function (item) {
//     var node = document.createElement("dt");
//     var textnode = document.createTextNode("name");
//     var node2 = document.createElement("dd");
//     var textnode2 = document.createTextNode(item.username);
//     node.appendChild(textnode);
//     node2.appendChild(textnode2);
//     document.getElementById("content").appendChild(node);
//     document.getElementById("content").appendChild(node2);
// };

// simple test for simple functions
describe('hi ordered functions test', function () {
        describe('map function test', function () {
            it('should return data ordered by filed name', function () {
            assert.deepEqual(data.map(mapCallback), ["LILLIAN", "MARSH", "BIRD"]);
             });
        }),

        describe('sort function test', function () {
            it('should return array ordered by filed name', function () {
                assert.deepEqual(data.sort(sortCallback), sortResult);
            });
        }),

        describe('filter function test', function () {
            it('should return array with elements were age over 28', function () {
                assert.deepEqual(data.filter(filterCallback), filterResult);
            });
        }),

        describe('reduce function test', function () {
            it('should return all element name lenght', function () {
                assert.deepEqual(data.reduce(reduceCallback, 0), 16);
            });
        }),

        describe('forEach function test', function () {
            it('should modified name for all elements name add new ', function () {
                data.forEach(foreachCallback)
                assert.deepEqual(data, foreachResult);
            });
        })
});



