var logic = {
    mapCallback: mapCallback,
    sortCallback: sortCallback,
    filterCallback: filterCallback,
    reduceCallback: reduceCallback,
    foreachCallback: foreachCallback
}
var mapCallback = function (a) {
    return a.name.toUpperCase();
};

var sortCallback = function (a, b) {
    if (a.name > b.name) {
        return 1;
    } else {
        return -1;
    }
};

var filterCallback = function (a) {
    return a.age > 28;
};

var reduceCallback = function myReduce(prev, item) {
    return prev + item.name.length;
};

var foreachCallback = function (item) {
    item.name = item.name + "new";
};

// this foreache work with dom elements accotrding to task
// var foreachCallback = function (item) {
//     var node = document.createElement("dt");
//     var textnode = document.createTextNode("name");
//     var node2 = document.createElement("dd");
//     var textnode2 = document.createTextNode(item.username);
//     node.appendChild(textnode);
//     node2.appendChild(textnode2);
//     document.getElementById("content").appendChild(node);
//     document.getElementById("content").appendChild(node2);
// };